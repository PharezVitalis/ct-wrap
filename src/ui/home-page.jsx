import React from 'react';
import { Container } from 'semantic-ui-react';
import PageHeader from './page-header';
import Search from './search';



const HomePage = ()=><>
      <PageHeader />
   <Container padded="very"  >
      <Search />
   </Container>
   </>

export default HomePage;




