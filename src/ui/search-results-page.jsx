import React from 'react';
import { Segment, Container, Icon, Button, Header, Modal, CardGroup, Pagination, Input, Progress } from 'semantic-ui-react';
import { performRequest, queryToFile, searchToQueryString } from '../api-connector/ct-client';
import DownloadModal from './download-modal';
import PageHeader from './page-header';
import Search, { SEARCH_SESSION_KEY, defaultSearchState } from './search';
import StudyCard from './study-card';

//const readable = new ReadableStream();
const RESULTS_SESSION = 'sResulstsSession';//used to tack on pagination.



export default class SearchResultsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showSearchModal: false,
            search: defaultSearchState(),
            searchResults: [],
            isSearching: true,
            isDownloading: false,
            dateOfSearch: 1000,
            pagination: {
                currentPage: 1,
                maxPages: 1,
                resultsCount: 0
            },
            pageSkip: 1,
            downloadProgress: 0,
            e: null,
            downloadModalOpen:false
        }
        this.searchRequest = null;
        this.downoadPromise = null;
        // this.contextRef = createRef();
    }

    componentDidMount() {
        const search = JSON.parse(sessionStorage.getItem(SEARCH_SESSION_KEY)) || defaultSearchState();
        const pagination = JSON.parse(sessionStorage.getItem(RESULTS_SESSION)) || { currentPage: 1, maxPages: 1, resultsCount: 0 };
        this.setState({ search, pagination }, this.performSearch);
    }

    performSearch = () => {
        const { search, pagination } = this.state;
        const url = searchToQueryString(search, pagination.currentPage, 'study_fields');
        if (this.searchRequest) {
            this.searchRequest.destroy();
        }
        this.setState({ dateOfSearch: Date.now(), isSearching: true });
        Promise.resolve(performRequest(url, this.onSearchEnd)).then(v => {
            this.searchRequest = v;
        });

    }

    onSearchEnd = (err, buffer) => {
        if (err) {
            console.error(err);
            this.setState({ isSearching: false });
            return;
        }
        const { search, pagination } = this.state;
        let resultsObj = null;
        try {
            resultsObj = JSON.parse(buffer) || null;
           // console.log(resultsObj);
        }
        catch (e) {
            console.error(e)
            console.log(resultsObj);
            this.setState({ e , isSearching:false});
            return;
        }

        if (!resultsObj) return;
        const { StudyFieldsResponse = {} } = resultsObj;
        const { MinRank: minRank, NStudiesFound: resultsCount =0, StudyFields = [] } = StudyFieldsResponse;
        const { skip, perPage } = search;
        const unskippedResults = resultsCount - skip;
        pagination.maxPages = Math.ceil(unskippedResults / perPage);
        pagination.resultsCount = unskippedResults;

        if (minRank > skip) {
            pagination.currentPage = 1 + (Math.ceil((minRank - (skip + 1)) / perPage));

        }
        else {
            pagination.currentPage = 1;
        }

        const searchResults = StudyFields.map(s => {
            const formatted = {};
            for (const key in s) {
                const value = s[key];
                if (typeof value !== 'string' && Array.isArray(value)) {
                    if (value.length === 1) {
                        formatted[key] = value[0]
                    }
                    else if (value.length > 0) {
                        formatted[key] = value;
                    }
                    else {
                        continue
                    }
                }
                else {
                    formatted[key] = value;
                }
            }
            return formatted;
        })
        this.setState({ pagination, pageSkip: pagination.currentPage, searchResults, isSearching: false }, () => setTimeout(() => this.forceUpdate(), 6000));
    }

    onSetSearchModal = (open) => this.setState({ showSearchModal: open });

    onSubmitSearch = (search) => {
        this.setState({ search, showSearchModal: false }, () => {
            sessionStorage.setItem(SEARCH_SESSION_KEY, JSON.stringify(this.state.search));
            this.performSearch();
        });
    }

    componentWillUnmount() {
        if (this.searchRequest) {
            this.searchRequest.destroy();
        }
    }

    onChangeActivePage = (e, { activePage }) => {
        const pagination = { ...this.state.pagination };
        if(pagination.currentPage === activePage)return;
        pagination.currentPage = activePage;
        this.setState({ pagination, isSearching: true }, this.performSearch);;
    }

    onSubmitDownload = (options ={} ) => {
        if (this.downloadPromise) return;
        this.setState({ isDownloading: true, downloadModalOpen:false });
        const { search } = this.state;
        const {searchExpression, fields, skip = 0, searchField, } = search;
        const query = {searchExpression, fields, skip, searchField};
        const methods = { onComplete: this.onDownloadComplete, onProgress: this.onDownloadProgress, onError: this.onDownloadErorr };
        options.methods = methods;        
        this.downloadPromise = queryToFile(query, options);
    }

    onDownloadProgress = (percent) => {
        this.setState({ downloadProgress: percent });
    }

    onDownloadErorr = (e) =>{
        this.setState({ e });
    }

    onDownloadComplete = (e, r) => {
        this.downloadPromise = null;
        this.setState({ isDownloading: false });
    }

    render() {
        const { downloadProgress, pageSkip, showSearchModal, dateOfSearch, search = {}, pagination, isDownloading,
            searchResults = [], isSearching, downloadModalOpen } = this.state;
        const { onSetSearchModal } = this;
        let resultsSegment = null;
        let labelSize = "big";

        if (search.fields.length > 10) {
            labelSize = "medium";
        }

        let searchExpression = '', skip = 0;

        if (isSearching) {
            resultsSegment = (<Container textAlign="center" text >
                <Segment textAlign="center">
                    <Icon.Group size='big'>
                        <Icon color="blue" loading size='massive' name='spinner' />
                        <Icon name='search' size="huge" />
                    </Icon.Group>
                    <Header as="h2" color="blue" textAlign="center">Getting Results</Header>
                </Segment>
            </Container>);
        }
        else if (searchResults.length < 1) {
            resultsSegment = (<Container textAlign="center" text >
                <Segment padded="very" textAlign="center">
                    <Icon color="red" size='massive' name='ban' />
                    <Header as="h2"> No Results Found</Header>
                </Segment>
            </Container>);
        }
        else {
            ({ searchExpression, skip } = search);
            
            resultsSegment = (<>
                <Container style={{ maxHeight: 600, overflowY: "auto" }} textAlign="center" text >
                    <Segment padded="very" textAlign="center">
                        <CardGroup color="grey">
                            {searchResults.map((study, idx) => {
                                return <StudyCard labelSize={labelSize} hideDefaults={false} key={`results-study-${idx}`} study={study} />
                            })}
                        </CardGroup>
                    </Segment>
                </Container></>);
        }
        const displayExpression = searchExpression.length > 0 ? searchExpression : '(All)';

        const showRefresh = Date.now() - dateOfSearch > 5000;
        const { currentPage, maxPages = 1, resultsCount = 0 } = pagination;
        return <div ref={this.contextRef}>
            <PageHeader />
            <Container textAlign="center" text >
                <Segment>
                    <Header as="h1" color="blue">
                        <Button title="Show search modal" as="span" color="blue" inverted onClick={() => this.onSetSearchModal(true)} padded="true" icon="search" size="massive" textAlign="center" />
                        {displayExpression}
                    </Header>
                    {!isSearching ? <>
                        <Header textAlign="center" color="blue" as="h5">Search Performed at: {new Date(dateOfSearch).toLocaleString()}
                            {showRefresh ? <Button style={{ "marginLeft": 6 }} basic padded="very" circular title="Refresh" icon="sync" color="blue" onClick={() => this.performSearch()} /> : null}
                        </Header>
                        <Header textAlign="center" color="blue" as="h5">{resultsCount} Studies Found and {skip} skipped.</Header>
                        <Pagination as="div" onPageChange={this.onChangeActivePage} totalPages={maxPages} activePage={currentPage} />
                        <span className="mr6">
                            <Input size="small" type="number" labelPosition="left" label="Skip To Page" value={pageSkip} onChange={(e) => this.setState({ pageSkip: parseInt(e.target.value) })} />
                            <Button size="small" disabled={pageSkip > maxPages} icon="arrow alternate circle right" color="blue" inverted onClick={() => this.onChangeActivePage(null, { activePage: pageSkip })} />
                        </span>

                        {isDownloading ? <>
                        <br />
                            <Progress percent={downloadProgress} size="small" color="green"  >
                                {`${downloadProgress}% Downloading`}
                            </Progress>
                            <Icon.Group title="processing downloading" size="huge">
                                <Icon color="grey" name="circle notch" loading />
                                <Icon color="blue" name="download" />
                            </Icon.Group> </> 
                            : <>
                            <Button title="Download this search" as="div" onClick={()=>this.setState({downloadModalOpen:true})} size="large" icon="download" inverted color="green" />
                            <DownloadModal search={search}  onSubmit={this.onSubmitDownload} onCancel={()=>this.setState({downloadModalOpen:false})} open={downloadModalOpen}/>
                            </>
                        }


                    </> :
                        <Icon color="blue" size='large' name="spinner" loading />
                    }
                </Segment>
            </Container>
            {resultsSegment}
            <Container textAlign="center" text compact>
                <Modal basic size="large" closeIcon={true} closeOnEscape dimmer="blurring"
                    onClose={() => onSetSearchModal(false)}
                    onOpen={() => onSetSearchModal(true)}
                    open={showSearchModal}
                >
                    <Search onSubmit={this.onSubmitSearch} lockSession />
                </Modal>
            </Container>
        </div >
    }
}
