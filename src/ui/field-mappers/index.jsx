import { capitalize } from "lodash";
import { useContext, useState } from "react";
import { Label, Button } from "semantic-ui-react";
import { FieldsContext } from "../../contexts";

const maxTextLength = 30;

export const TypeMap = {
  'string':(fieldValue)=>{
    return capitalize(fieldValue);
  },
  'number':(fieldValue)=>{
    return '#'+fieldValue.toString();
  },
  'object':(fieldValue)=>{
    let outStr = '';
    const keys = Object.keys(fieldValue);
    if(keys.length>0){
      const previousValues = {};
      outStr = capitalize(fieldValue[keys[0]])||'';
      previousValues[ fieldValue[keys[0] ] ] = true;

      for(let i = 1; i<keys.length; i++){
        const key = keys[i];
        const value = fieldValue[key];
        
        if(previousValues[value]){
          continue;
        }

        previousValues[value ] =true;
        outStr += ' || '+capitalize(value);
      }
    }
    return outStr;
  },

}

const StudyField = ({ size='big', fieldKey = '', fieldValue = null }) => {
    const fields = useContext(FieldsContext);
    const fieldProp = fields.find(({value})=>value === fieldKey)
    const fieldName = fieldProp? fieldProp.text:fieldKey;
    const displayText =fieldValue !== null? TypeMap[typeof fieldValue](fieldValue): '';

    const [hidden, setState ] = useState(displayText.length>maxTextLength? true:null);
    
    let labelValueDisp = null;
    if(hidden === true){
      labelValueDisp = <>
      <Button title="View Full" onClick={()=>setState(false)} inverted color="grey" compact icon="eye slash outline" as="span" />
      {`${displayText.substr(0,maxTextLength)}...`}
       </>;
    }
    else if(hidden === false){
      labelValueDisp = <>
      <Button title="Hide" onClick={()=>setState(true)} compact inverted color="green" icon="eye" as="span" />
      {displayText}
       </>;
    }
    else{
      labelValueDisp = <span>{displayText}</span>
    }
    //console.log(hidden);
    return <>
    <Label size={size} as='a' color='blue' image>
      {labelValueDisp}
      <Label.Detail>{fieldName}</Label.Detail>
    </Label>
    </>
}

export default StudyField;