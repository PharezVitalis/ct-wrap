import { navigate } from "hookrouter";
import { isEqual, toLower } from "lodash";
import { Component, useEffect, useState } from "react";
import { Container, List, Input, Icon, Label, Segment, Header, Button, Dropdown, Modal, Card, } from "semantic-ui-react";
import { getFieldText } from "../App";
import { FieldsContext } from '../contexts';

export const SEARCH_SESSION_KEY = "currentSearch";
export const HISTORY_LOCAL_KEY = "searchHistory";
export const MAX_FIELDS_LENGTH = 20;
export const defaultFields = ['NCTId', 'BriefTitle', 'LastUpdateSubmitDate'];
const MAX_SEARCH_HISTORY = 10

export const defaultSearchState = () => {
    return {
        searchExpression: '',
        fields: [...defaultFields],
        skip: 0,
        perPage: 100,
        searchField: {}
    }
}

const SearchFieldModal = (props) => {
    const { context, initialSearch = '', onSubmit, onDismiss, open } = props;
    const [text, setState] = useState(initialSearch);
    useEffect(() => {
        setState(initialSearch);
    }, [props.context, props.open, initialSearch]);

    return (<Modal closeIcon={true} closeOnEscape dimmer="blurring" onClose={onDismiss} open={open}>
        {text.length > 0 ? <Button color="red" inverted icon="trash" title="Remove this search" onClick={() => {
            onSubmit('');
            onDismiss();
        }} /> : null
        }
        <Header textAlign="center" as="h3"> Search <em>"{context}"</em> Field</Header>
        <Input fluid value={text} onChange={(e) => setState(e.target.value)} />
        <br />
        <div className="mxa">
            <Button onClick={() => onDismiss()} icon="cancel" inverted color="red" />
            <span className="mx3" />
            <Button onClick={() => onSubmit(text)} icon="check circle outline" inverted color="green" />
        </div>
    </Modal>)
}

const FieldsListIconModal = (props) => {
    const { fields, allFields, open, onDismiss, onAddField, onRemoveField } = props;
    const [search, setState] = useState('');
    const srchNormalised = toLower(search);
    return (<Modal closeIcon={true} closeOnEscape dimmer="blurring" onClose={onDismiss} open={open}>
        <Header as="h3"> All Available Search Fields</Header>
        <Header as="h5">{fields.length} Fields Selected (up to {MAX_FIELDS_LENGTH} allowed per search)</Header>
        <Input fluid size="big" icon="search" onChange={e => setState(e.target.value)} />
        <div style={{ maxHeight: 800, overflowY: 'auto' }}>
            <List relaxed verticalAlign="middle" celled >
                {allFields.map(({ value, text, area }, idx) => {
                    if (srchNormalised.length > 0) {
                        const normalised = toLower(text);
                        if (!srchNormalised.includes(normalised) && !normalised.includes(srchNormalised)) return null;
                    }
                    const active = fields.some(f => f === value);
                    let icon = '', color = '', onChange = null;
                    if (active) {
                        if (defaultFields.includes(value)) {
                            color = 'grey';
                        }
                        else {
                            color = "green";
                            onChange = () => onRemoveField({ value });
                        }
                        icon = 'eye';
                    }
                    else {
                        icon = "eye slash outline";
                        color = "grey";
                        onChange = () => onAddField(value);
                    }

                    return (<List.Item key={`modal-fields-${idx}`}>
                        <Button title="Add this field to your search" onClick={onChange} icon={icon} color={color} />
                        <span className="ml3">{text}</span>
                    </List.Item>)
                })}
            </List>
        </div>
    </Modal>)
}

const PerPages = [{ value: 25, text: "25", key: 25 }, { value: 50, text: "50", key: 50 }, { value: 75, text: "75", key: 75 }, { value: 100, text: "100", key: 100 }];

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: defaultSearchState(),
            searchFieldOpen: false,
            fieldListOpen: false,
            activeFieldTerm: '',
            activeSearchObj: {},
            fieldSearch: '',
            history: [],
            historyEnabled: true
        }
    }

    componentDidMount() {
        this.setStateToSession();
    }

    onAddField = (e) => {
        e.preventDefault();
        const { fieldSearch = '' } = this.state;
        if (!fieldSearch.length) {
            return;
        }
        const AllFields = this.context;

        const relevantField = AllFields.find(({ value, text }) => text === fieldSearch);

        if (!relevantField) {
            return;
        }
        const { value } = relevantField;
        if (defaultFields.some(f => f === value)) {
            return;
        }
        const search = { ...this.state.search };

        const { fields } = search;
        if (fields.length >= MAX_FIELDS_LENGTH) return;
        if (fields.some(f => f === value)) {
            return;
        }
        fields.push(value);

        this.setState({ search, fieldSearch: '' }, this.onSetSession);
    }

    setStateToSession = () => {
        const sessionJson = sessionStorage.getItem(SEARCH_SESSION_KEY);
        const stateUpdate = { search: { ...defaultSearchState(), ...JSON.parse(sessionJson) } };
        const historyJson = localStorage.getItem(HISTORY_LOCAL_KEY) || '';
        if (historyJson.length > 2) {
            try {
                const { history = [], historyEnabled = true } = JSON.parse(historyJson) || {};
                stateUpdate.history = history;
                stateUpdate.historyEnabled = historyEnabled;
            }
            catch (e) {
                console.error("Failed To Get Search History" + e);
                localStorage.setItem(HISTORY_LOCAL_KEY, null);
                return;
            }
        }
        this.setState(stateUpdate);
    }

    onPushField = (value) => {

        const search = { ...this.state.search };

        const { fields } = search;
        if (fields.length >= MAX_FIELDS_LENGTH) return;
        if (fields.some(f => f === value)) {
            return;
        }
        fields.push(value);

        this.setState({ search }, this.onSetSession);
    }

    onRemoveField = ({ idx = -1, value }) => {
        const search = { ...this.state.search };
        const { fields, searchField } = search;
        if (idx > -1) {
            const field = fields[idx];
            if (defaultFields.includes(field)) {
                return;
            }
            if (!!searchField[field]) {
                delete searchField[field];
            }
            fields.splice(idx, 1);
        }
        else if (value.length > 0) {
            if (defaultFields.includes(value)) {
                return;
            }
            idx = fields.findIndex(f => f === value);
            if (idx < 0) return;
            if (!!searchField[value]) {
                delete searchField[value];
            }
            fields.splice(idx, 1);
        }
        else return;

        this.setState({ search }, this.onSetSession)
    }

    onChangeFieldSearch = (e) => {
        this.setState({ fieldSearch: e.target.value });
    }

    onChangeSearchExpression = (e) => {
        const search = { ...this.state.search };
        search.searchExpression = e.target.value;
        this.setState({ search }, this.onSetSession);
    }

    onChangeSkip = (e) => {
        const search = { ...this.state.search };
        search.skip = parseInt(e.target.value)
        this.setState({ search }, this.onSetSession);
    }

    onChangePerPage = (e, data) => {
        const search = { ...this.state.search };
        let { value = '' } = data;
        search.perPage = value;
        this.setState({ search }, this.onSetSession);
    }

    onSetSession = () => {
        const { lockSession = false } = this.props;
        if (lockSession) return;
        const sessionString = JSON.stringify(this.state.search);
        sessionStorage.setItem(SEARCH_SESSION_KEY, sessionString);
    }


    onSubmit = (e) => {
        e.preventDefault();
        const { onSubmit } = this.props;
        const { search } = this.state;
        this.onAppendHistory(search)

        if (!!onSubmit) {
            onSubmit(search);
        }
        else {
            navigate('/search');
        }

    }

    onOpenFieldSearch = (fieldObj) => {
        const { value } = fieldObj;
        const stateUpdate = { searchFieldOpen: true, activeSearchObj: fieldObj };

        const { searchField } = this.state.search;
        stateUpdate.activeFieldTerm = searchField[value] || '';
        this.setState(stateUpdate);
    }

    onClear = (e) => {
        e.preventDefault();
        this.setState({ search: { ...defaultSearchState() } }, this.onSetSession);
    }

    onClearFieldSearch = (fieldValue) => {
        const search = { ...this.state.search };
        const { searchField } = search;
        if (!searchField[fieldValue]) return;
        delete searchField[fieldValue];
        this.setState({ search }, this.onSetSession);
    }

    stateToHistory = () => {
        const { history = [], historyEnabled } = this.state;
        const historyStr = JSON.stringify({ history, historyEnabled });
        localStorage.setItem(HISTORY_LOCAL_KEY, historyStr);
    }

    onAppendHistory = (search) => {
        const { history, historyEnabled } = this.state;
        if (!historyEnabled) return;
        for (let i = 0; i < history.length; i++) {
            const nextItem = history[i];
            if (isEqual(nextItem, search)) {
                history.splice(i, 1);
            }
        }
        history.unshift(search);

        if (history.length > MAX_SEARCH_HISTORY) {
            history.splice(MAX_SEARCH_HISTORY - 1, history.length - MAX_SEARCH_HISTORY);
        }

        const historyString = JSON.stringify({ history, historyEnabled });
        localStorage.setItem(HISTORY_LOCAL_KEY, historyString)
    }

    onSubmitSearchField = (fieldSearch) => {
        const search = { ...this.state.search };
        const { activeSearchObj } = this.state;
        const stateUpdate = { searchFieldOpen: false, activeSearchObj: {}, activeFieldTerm: '' };

        if (fieldSearch.length > 0) {
            search.searchField[activeSearchObj.value] = fieldSearch;
            stateUpdate.search = search;
        }
        else if (search.searchField[activeSearchObj.value]) {
            delete search.searchField[activeSearchObj.value];
            stateUpdate.search = search;
        }
        this.setState(stateUpdate, this.onSetSession);
    }

    onDismissSearchField = () => this.setState({ searchFieldOpen: false, activeFieldTerm: '', activeSearchObj: {} });

    onStarHistory = (idx) => {
        const history = [...this.state.history];
        const searchItem = history[idx];
        const { starred = false } = searchItem;
        searchItem.starred = !starred;
        history.sort((a, b) => {
            const { starred: aStarred = false } = a;
            const { starred: bStarred = false } = b;
            return bStarred - aStarred;
        });
        this.setState({ history }, this.stateToHistory);
    }

    onHistoryClear = () => {
        const history = [...this.state.history].filter(({ starred = false }) => starred);
        this.setState({ history }, this.stateToHistory);
    }

    onHistoryClick = (idx) => {
        const search = this.state.history[idx];
        this.setState({ search }, this.onSetSession);
    }

    onRemoveHistory = (idx) => {
        const history = [...this.state.history];
        history.splice(idx, 1);
        this.setState({ history }, this.stateToHistory);
    }

    render() {
        const { fieldListOpen, search, fieldSearch, searchFieldOpen, activeSearchObj, activeFieldTerm, history = [], historyEnabled } = this.state;
        const { searchExpression, fields, skip, perPage, searchField = {} } = search;

        const AllFields = this.context;

        const isValidField = AllFields.some(({ text }) => text === fieldSearch);
        const studyLabel = skip === 1 ? "Study" : "Studies";

        return <Container textAlign="center" >
            <Segment textAlign="center">
                <Header as="div">Search History</Header>
                <Button.Group>
                    {history.length>0?<Button size="large" title="Clear search history" inverted color="red" as="span" onClick={() => this.onHistoryClear()} icon="erase" />:null}
                    <Button toggle active={historyEnabled} size="large" title="Toggle History" icon="history" onClick={() => this.setState({ historyEnabled: !historyEnabled }, this.stateToHistory)} />
                </Button.Group>
                {history.length > 0 && historyEnabled ?
                    <Card.Group textAlign="center" style={{ maxHeight: 250, marginTop: "1em", overflowX: 'auto' }} className="ui four doubling stackable cards" >
                        {history.map((s, idx) => <SearchCard idx={idx} onFavourite={this.onStarHistory} isActive={isEqual(s, search)} onApply={this.onHistoryClick} onClear={this.onRemoveHistory} search={s} key={`search-hist-c-${idx}`} />)}
                    </Card.Group> : null}
            </Segment>

            <Segment padded="very" raised textAlign="center" >
                <Icon name="search" size="massive" color="blue" onClick={this.onSubmit} />
                <Input onChange={this.onChangeSearchExpression} value={searchExpression}
                    placeholder="Main Search Term" size="massive" />
                <Button size="medium" icon="ban" color="red" onClick={this.onClear} title="Clear All" floated="right" />
            </Segment>
            <Segment clearing>
                <Header as="h3" textAlign="center">Included Fields</Header>
                <Input value={fieldSearch} onChange={this.onChangeFieldSearch} icon="tags" iconPosition="left" list="all-fields" labelPosition='right' placeholder="Add a field To Show" />
                <datalist id="all-fields">
                    {AllFields.map(({ text, value }, idx) => {
                        if (fields.some(field => field === value)) return null;
                        return <option key={`all-fields-d-list-${idx}`} value={text}>{text}</option>
                    })}
                </datalist>
                <Button onClick={this.onAddField} enabled={"" + (isValidField && fields.length < MAX_FIELDS_LENGTH)} title="Add Field" size="large" icon="search plus" />
                <Button icon="eye" size="big" onClick={() => this.setState({ fieldListOpen: !fieldListOpen })} />
                {fields.length >= MAX_FIELDS_LENGTH ? <Header as="h4" color='yellow'> You Can only have up to {MAX_FIELDS_LENGTH} fields on a search</Header> : null}
                <Container>
                    {fields.map((value, idx) => {
                        const isDefault = defaultFields.includes(value);
                        const field = AllFields.find(({ value: v }) => value === v);
                        if (!field) return null;
                        const { text: fieldText } = field;
                        let onRemoveField = null, color = '', title = '';
                        if (isDefault) {
                            color = "grey";
                            title = "This field is required";
                        }
                        else {
                            color = "blue";
                            onRemoveField = (e) => this.onRemoveField({ idx });
                            title = "This field will appear in the search results";
                        }
                        const searchTerm = searchField[value];
                        const hasSearchTerm = !!searchTerm;
                        let displayText = fieldText;
                        let size = 'medium';
                        if (hasSearchTerm) {
                            displayText = `${fieldText} = "${searchTerm}"`;
                            color = "green";
                            size = "big";
                        }

                        return <Label size={size} title={title} color={color} key={`field-tag-${idx}`} as="span" tag>
                            <span title="Add a search to this field" className="pointer" onClick={() => this.onOpenFieldSearch(field)}>
                                <Button size="tiny" compact inverted color="grey" icon="search" />
                            </span>
                            {displayText}
                            {hasSearchTerm ? <Button title="Clear the query" size="tiny" onClick={() => this.onClearFieldSearch(value)} icon="ban" color="red" inverted /> : null}
                            {!isDefault ? <Icon title={`Remove '${fieldText}' from appearing in search results`} name="delete" onClick={onRemoveField} /> : null}
                        </Label>
                    })}
                </Container>
            </Segment>
            <Segment>
                <Header as="h3" textAlign="center">Pagination</Header>
                <Container>
                    <Label size="big" basic >Skip the First</Label>
                    <Input onChange={this.onChangeSkip} min="0" value={skip} type="number" size="big" placeholder="(Amount of)" label={{ basic: true, content: studyLabel }} labelPosition="right" />
                    <Label size="big" basic >Studies Per Page</Label>
                    <Dropdown value={perPage} selection placeholder="Studies Per Page" options={PerPages} onChange={this.onChangePerPage} />
                </Container>
            </Segment>
            <Button onClick={this.onSubmit} icon="search" size="massive" color="blue">Go</Button>
            <FieldsListIconModal onAddField={this.onPushField} onRemoveField={this.onRemoveField} open={fieldListOpen} onDismiss={() => this.setState({ fieldListOpen: false })} fields={fields} allFields={AllFields} />
            <SearchFieldModal initialSearch={activeFieldTerm} onSubmit={this.onSubmitSearchField} context={activeSearchObj.text} open={searchFieldOpen} onDismiss={this.onDismissSearchField} />
        </Container>
    }

}


const SearchCard = ({ search, idx, onApply, onClear, onFavourite, isActive = true }) => {
    const [showAllFields, setState] = useState(false);
    if (!search) return null;
    const { fields, searchExpression, skip = 0, starred = false } = search;

    let mainText = searchExpression.length ? searchExpression : "(ALL)";
    if (skip > 0) {
        mainText += `(${skip} skipped)`;
    }

    const limit = !showAllFields ? 4 : fields.length
    const fieldContent = [];
    let count = 0;

    for (let i = 0; i < fields.length && count < limit; i++) {
        const nextField = fields[i];
        if (defaultFields.includes(nextField)) continue;
        count++;
        fieldContent.push(<Label key={`search-card-fc-${count}`}>{getFieldText(nextField)}</Label>)
    }
    const showButtonProps = showAllFields ? { color: 'blue', icon: 'eye' } : { color: 'grey', icon: 'eye slash' };
    const starButtonProps = starred ? { icon: "star", color: "blue" } : { icon: "star outline", color: "grey" };

    return <Card as="span">
        <Button title="Apply this search" onClick={() => onApply(idx)} color={isActive ? "blue" : "grey"} size="huge" >{mainText}</Button>
        <span className="my1 inline">
            <Button compact as="span" color="red" icon="erase" onClick={() => onClear(idx)} />
            {fields.length - defaultFields.length > 4 ? <Button compact as="span" {...showButtonProps} onClick={() => setState(!showAllFields)} /> : null}
            <Button onClick={() => onFavourite(idx)} compact as="span"{...starButtonProps} />
        </span>
        <Card.Content title="Fields">
            {fieldContent}
        </Card.Content>
    </Card>
}

Search.contextType = FieldsContext;

export default Search;