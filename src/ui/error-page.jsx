import { Button, Container, Header, Icon } from "semantic-ui-react"
import PageHeader from './page-header';
import React from 'react';

export default class ErrorPage extends React.Component {

    state = {
        error:null,
        hasError:false
    }

    componentDidCatch(error) {
        console.warn("The following error crashed the app")
        console.error(error);
        this.setState({hasError:true, error});
      }

    render() {
        const { hasError} = this.state;
        const {   children } = this.props
        if (!hasError) return <>{children}</>

        return (<>
            <PageHeader />
            <Container fluid textAlign="center">
                <Icon size="massive" color="red" name="warning circle" />
                <Header as="h1" color="red"> Something Went Wrong</Header>

                <Button title="Title Reload Page" icon="sync" 
                    onClick={() => window.location.reload()} />
            </Container>
        </>)
    }

}