import React from 'react';
import Qs from 'query-string';
import { performRequest, searchToQueryString } from '../api-connector/ct-client';
import PageHeader from './page-header';
import { Button, Container, Dropdown, Header, Icon, Segment } from 'semantic-ui-react';
import ModuleMap, { moduleDropdownMap as TabsMap } from './page-modules/';
import { capitalize, get, map } from 'lodash';
import Joi from 'joi';
import { A } from 'hookrouter';
import { useQueryParams } from 'hookrouter';

const titleCase = (str) => map(str.split(" "), capitalize).join(" ");
const Summary = ModuleMap['!Summary'];

const SESSION_NAME = 'STUDY_PAGE_SESSION';

const DropDownMaps = TabsMap.filter(({key})=>key !== 'summary');

export default class StudyPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            study: {},
            currentModules: ['DerivedSection.ConditionBrowseModule'],
            invalidId: false,
            showFullDescription: false,
            showOfficialTitle: false,
        }
        this.NCTId = props.NCTId || null;
        this.request = null;
    }

    getStudy = () => {
       const {NCTId} = this.props;
       if(!NCTId)return;

        const search = { searchExpression: NCTId, fmt: "json" };
        const queryUrl = searchToQueryString(search, 1, 'full_studies');
        if (this.request) {
            this.request.destroy();
        }
        this.setState({ loading: true })
        this.NCTId = NCTId;
        Promise.resolve(performRequest(queryUrl, this.onReceivedStudy)).then(v=>this.request = v);
    }

    onReceivedStudy = (err, result) => {
        if (err) {
            this.setState({ loading: false });
            return;
        }
        const receivedObj = JSON.parse(result);
        //console.log(result);
        const { FullStudiesResponse: responseObj } = receivedObj;
        const { NStudiesReturned: count, FullStudies: studies = [] } = responseObj;
        if (count !== 1) {
            this.setState({ loading: false });
            return;
        }
        const study = studies[0].Study;
        study.fetchedAt = Date.now();
        this.setState({ loading: false, study });
    }

    componentDidUpdate(){
        const {NCTId} = this.props;
        if(NCTId === this.NCTId) return;
        this.getStudy();
    }

    componentDidMount() {
        this.onApplySession();
        this.getStudy();
    }

    onApplySession=()=>{
        const session = sessionStorage.getItem(SESSION_NAME) || '';
        if(session.length<2)return;
        try{
            const stateUpdate = JSON.parse(session);
            this.setState(stateUpdate);
        }
        catch(e){
            console.error("Failed to apply session to state due to error below")
            console.error(e);
        }
        
        
    }

    onSaveSession=()=>{
        //could use this to allievate serverload
        const {currentModules, showFullDescription, showOfficialTitle} = this.state;
        const sessionUpdate = {currentModules, showFullDescription, showOfficialTitle};
        sessionStorage.setItem(SESSION_NAME, JSON.stringify(sessionUpdate));
    }

    componentWillUnmount() {
        if (this.request) {
            this.request.destroy();
        }
    }

    render() {
        const { study, loading, currentModules, showOfficialTitle } = this.state;

        let studyContent = null;

        if (loading) {
            //loading
            studyContent = <Segment textAlign="center">
                <Icon size="massive" color="blue" loading name="spinner" />
            </Segment>;
        }
        else if (!study.ProtocolSection) {
            const { invalidId } = this.state;
            
            studyContent = <Segment textAlign="center">
                <Icon size="massive" color="red" name="ban" />
                {invalidId ? <>
                    <Header color="red" textAlign="center" as="h3"> The NCT ID supplied was invalid</Header>
                    <Header color="red" textAlign="center" as="h4"> should be NCT then 8 digitas <em>e.g. NCT12345678</em></Header>
                </> :
                    <Header textAlign="center" as="h2"> Study Not found</Header>}
                <A title="Go Home" href='/'><Button icon="home" color="blue" compact size="huge" /></A>
            </Segment>;
        }
        else {
            const { BriefTitle: briefTitle, OfficialTitle: ofTitle } = study.ProtocolSection.IdentificationModule;
            studyContent = <>
                <Segment textAlign="center">
                    <Header as="h2">{titleCase((showOfficialTitle ? ofTitle : briefTitle))}</Header>
                    <Button onClick={() => this.setState({ showOfficialTitle: !showOfficialTitle })} title="click to change title shown" >
                        <Icon name="pencil" />
                        {showOfficialTitle ? 'Official Title' : 'Brief Title'}
                    </Button>
                </Segment>
                <Summary data={study} />
                <Segment>
                    <Header as="span">View</Header>
                    <Dropdown scrolling as="h3" multiple={true} options={DropDownMaps} value={currentModules} onChange={(e, data) => this.setState({ currentModules: data.value || [] }, this.onSaveSession)}  />
                </Segment>
                {currentModules.map((currentModule, idx) => {
                    if(currentModule === '!Summary') return null;
                    const data = (!currentModule.includes('!') ? get(study, currentModule) : study) || {};
                    const Module = ModuleMap[currentModule];
                    return <Module data={data} key={`study-page-mod-${idx}`} />
                })}
            </>
        }


        return <>
            <PageHeader />
            <Container textAlign="center">
                {studyContent}
            </Container>
        </>

    }
}

