import { Button, Card, Icon, Label } from "semantic-ui-react";
import React, { useState } from 'react';
import StudyField from "./field-mappers";
import { defaultFields } from './search';
import { A } from 'hookrouter';

export const getExternalLink = NCTId => `https://clinicaltrials.gov/ct2/show/${NCTId}`

const StudyCard = ({ study, labelSize, cardIndex, }) => {
    const [headerHover, onChangeHover] = useState(false);
    const { Rank, BriefTitle = '', NCTId = '', LastUpdateSubmitDate } = study;
    const displayTitle = (headerHover || BriefTitle.length < 120) ? BriefTitle : BriefTitle.substr(0, 120) + "...";
    const externalCardLink = `https://clinicaltrials.gov/ct2/show/${NCTId}`
    const internalLink = `/study/${NCTId}`;
    const studyKeys = Object.keys(study).filter(key => {
        if (key === "Rank") return false;
        if (defaultFields.some(f => f === key)) return false;
        return true;
    })
    return (<Card fluid>
        <Card.Content >
            <Card.Header onMouseLeave={() => onChangeHover(false)}
                onMouseEnter={() => onChangeHover(true)} as="h4">{displayTitle}</Card.Header>
            <Label size="big" title="Search Result Number" color="blue"><Icon name="hashtag" />{Rank}</Label>
            <Label title="ID" color="green"><Icon name="id badge outline" /> {NCTId} <Label.Detail>NCT Identifier</Label.Detail></Label>
            <Label title="Last Changed on CT" color="green"><Icon name="calendar alternate outline" /> {LastUpdateSubmitDate} <Label.Detail>Last Updated</Label.Detail></Label>
        </Card.Content>
        {studyKeys.length>0?
        <Card.Content>
            {studyKeys.map((key, idx) => {

                const value = study[key];
                return <StudyField size={labelSize} fieldKey={key} fieldValue={value} key={`resulsts-card-${cardIndex}-field-${idx}`} />
            })}
        </Card.Content>:null}
        <Card.Content textAlign="center">
            <A href={internalLink} title="View study"> <Button size="massive" color="blue" icon="info circle" inverted /></A>

            <a title="View on ct.gov" rel="noreferrer" target="_blank" href={externalCardLink}>
                <Button size="large" color="green" icon="external" inverted />
            </a>
        </Card.Content>

    </Card>);
}

export default StudyCard;