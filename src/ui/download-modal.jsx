
import React from 'react';
import { Radio, Modal,  Segment, Button, Icon, Header } from 'semantic-ui-react';
import { defaultQueryOptions, fileFormats } from '../api-connector/ct-client';

export default class DownloadModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options: { ...defaultQueryOptions }
        }
    }

    onUpdateOptions = (update) => {
        const options = { ...this.state.options, ...update };
        this.setState({ options });
    }

    onSubmit() {
        const { onSubmit, search } = this.props;
        if (onSubmit) {
            onSubmit(search);
        }
        else {
            // queryToFile() if necessary add code here
        }
    }

    getFormatRender = {
        'csv': (opts) => {
            return null
        },
        'json': (opts) => {
            return null
        },
        'xlsx': (opts, search) => {
            let { addCountryData = false, addStudyLocData = false } = opts;
            const canShowCountries = search.fields.some(f => f === "LocationCountry");
            addStudyLocData &= addCountryData;
            return <Segment>
                <Header as="h3" >Excel Export Options</Header>
                {canShowCountries ? <>
                    <Radio checked={addCountryData} onChange={() => this.onUpdateOptions({ formatOptions: { ...opts, addCountryData: !addCountryData } })} toggle label="Show Extra Country Data" />
                    <span className="ml3">
                        {addCountryData ? <Radio checked={addStudyLocData} onChange={() => this.onUpdateOptions({ formatOptions: { ...opts, addStudyLocData: !addStudyLocData } })} toggle label="Add Country / Language counts to studies" /> : null}
                    </span>
                </>
                    : <Header as="h3">Add "Location Country" field to add extra country data</Header>}
            </Segment>
        }
    }

    render() {
        const { open, search, onSubmit, onCancel } = this.props;
        const { options } = this.state;
        const { formatOptions = {}, fileFormat = 'csv' } = options;


        return <Modal onClose={() => onCancel()} open={open} basic closeIcon={true} closeOnEscape dimmer="blurring">

            <Segment>
                {fileFormats.map((fmt, idx) => {
                    const checked = fileFormat === fmt;
                    return <Radio onChange={() => this.onUpdateOptions({ fileFormat: fmt, formatOptions: {} })} name="formatSelection" label={fmt} key={`rad-fmt-${idx}`} value={fmt} checked={checked} />
                })}
                <Segment>
                    {/* Default Form Options */}
                </Segment>
                {this.getFormatRender[fileFormat](formatOptions, search)}
                <Button fluid primary onClick={() => onSubmit(options)} animated='vertical'>
                    <Button.Content hidden>Download</Button.Content>
                    <Button.Content visible>
                        <Icon name='download' />
                    </Button.Content>
                </Button>
            </Segment>
        </Modal>
    }
}