import { toLower } from 'lodash';
import React, { useState } from 'react';
import { Button, Card, Icon, Label } from 'semantic-ui-react';

const visibleCriterias = 8;


const EligibilityModule = ({ data }) => {
    const [showAllCriteria, setState] = useState(false);

    const { EligibilityCriteria: criteria = '', Gender: gender = '', GenderBased: genderBased,
        GenderDescription: genderDesc, HealthyVolunteers: healthyVolunteers, MinimumAge: minAge = "none", StdAgeList = {} } = data;
    const { StdAge: ages = [] } = StdAgeList;
    
    let genderLabel = null;
    if (gender === 'Male') {
        genderLabel = (<Label size="big">
            <Icon name="man" title="All" />
            Male
            <Label.Detail>Gender</Label.Detail>
        </Label>)

    }
    else if (gender === 'Female') {
        genderLabel = (<Label size="big">
            <Icon name="woman" title="All" />
            Female
            <Label.Detail>Gender</Label.Detail>
        </Label>)

    }
    else {
        genderLabel = (<Label size="big">
            <Icon name="users" title="All" />
            All
            <Label.Detail>Gender</Label.Detail>
        </Label>);
    }
    const criteriaList = criteria.split('\n');

    const criteriaDisplays = [];
    let count = 0;

    if (criteriaList.length < visibleCriterias) {
        count = criteriaList;
    }
    else {
        count = showAllCriteria ? criteriaList.length : visibleCriterias;
    }

    for (let i = 0; i < count; i++) {
        const text = criteriaList[i];
        criteriaDisplays.push(<p key={`criteria-${i}`}>{text}</p>);
    }

    const areHealthy = toLower(healthyVolunteers) === 'yes';

    let healthLabel = areHealthy ? <Label size="big"> <Icon title="The participants are health" name="heart" color="green" />
        <Label.Detail>Healthy Participants</Label.Detail>
    </Label> :
        <Label size="big">
            <Icon title="The participants must have a prexisiting condition" name="heart outline" color="yellow" />
            <Label.Detail>Must have a condition</Label.Detail>
        </Label>


    return <Card fluid>
        <Card.Content>
            <Card.Header textAlign="center" as="h3">
                Participant Eligibility
            </Card.Header>
        </Card.Content>
        <Card.Content>
            <Card.Header textAlign="center" as="h5">Criteria Outline</Card.Header>
            <Card.Description >{criteriaDisplays}</Card.Description>
            {criteriaList.length > visibleCriterias ?
                <Button onClick={() => setState(!showAllCriteria)} icon={`arrow ${showAllCriteria ? 'up' : 'down'}`} />
                : null}
        </Card.Content>

        <Card.Content>
            <Card.Header textAlign="center" as="h5">Age And Health</Card.Header>
            {healthLabel}
            <Label size="big">
                {minAge}
                <Label.Detail>Min Age</Label.Detail>
            </Label>
            <br />
            {ages.map((text,idx)=><Label color="grey" size="big" key={`age-group-${idx}`}>
                {text}
                <Label.Detail>Age Group</Label.Detail>
            </Label> )}
        </Card.Content>

            <Card.Content>
                <Card.Description textAlign="center" >
                    <Card.Header textAlign="center" as="h5">Gender Details</Card.Header>
                   <p>{genderDesc}</p>
                    <br />
                    {genderLabel}
                    {genderBased ==='Yes'?
                    (<Label title="Gender is a factor for eligibility" size="big">
                        <Icon name="venus mars"/>
                        <Label.Detail>Gender Based</Label.Detail>
                    </Label>):
                    null}
                </Card.Description>
            </Card.Content>
    </Card>
}

        export default EligibilityModule;