import { Card, Icon,  List } from "semantic-ui-react";
import React from 'react';

const OutcomeListItem = ({ baseKey = "PrimaryOutCome", outComeObj }) => {
    const description = outComeObj[`${baseKey}Description`] || '';
    const timeFrmame = outComeObj[`${baseKey}TimeFrame`] || '';
    const measure = outComeObj[`${baseKey}Measure`] || '';

    return <List.Item >
        <hr style={{ color: "rgba(0,0,0,0.65)", borderStyle: 'double' }} />
        <List.Header as="h4" title="Measure"> <Icon name="flask" size="big" /> {measure}</List.Header>
        <hr style={{ opacity: 0.4, borderStyle: 'groove' }} />
        <List.Description  title="Outcome Description" >
            <Icon size="large" name="indent" />
            <span>{description}</span>
        </List.Description>
        <hr style={{ opacity: 0.4, borderStyle: 'groove' }} />
        <List.Description title="Time Frame">
            <Icon size="large" name="clock" />
            <span>{timeFrmame}</span>
        </List.Description>
    </List.Item>

}

const flatten = (data) => {

    const { SecondaryOutcomeList: secondaryOutObj = {}, PrimaryOutcomeList: primaryOutListObj = {} } = data;
    const { SecondaryOutcome: secondaryOutcomes = [] } = secondaryOutObj;
    const { PrimaryOutcome: primaryOutcomes = [] } = primaryOutListObj;
    return { secondaryOutcomes, primaryOutcomes };
}


const OutcomesModule = ({ data }) => {
    const { secondaryOutcomes, primaryOutcomes } = flatten(data);

    return (<>
        <Card fluid>
            <Card.Header textAlign="center" as="h2"> Study Outcomes </Card.Header>
            <Card.Content>
                <Card.Header textAlign="center" as="h2"> Primary Outcomes </Card.Header>
                <List >
                    {primaryOutcomes.map((outComeObj, idx) => <OutcomeListItem key={`card-outcome-pri-${idx}`} baseKey="PrimaryOutcome" outComeObj={outComeObj} />)}
                </List>
            </Card.Content>
        </Card>
        {secondaryOutcomes.length > 0 ?
            <Card fluid>
                <Card.Header textAlign="center" as="h2"> Secondary Outcomes </Card.Header>
                <Card.Content extra>
                    <List >
                        {secondaryOutcomes.map((outComeObj, idx) => <OutcomeListItem key={`card-outcome-sec-${idx}`} baseKey="SecondaryOutcome" outComeObj={outComeObj} />)}
                    </List>
                </Card.Content>
            </Card> : null}
    </>
    )
}



export default OutcomesModule;