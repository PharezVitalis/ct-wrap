import { capitalize } from 'lodash';
import React, { useState } from 'react';
import { Container, Button, Segment, Label, Icon, Header } from 'semantic-ui-react';
import { getExternalLink } from '../study-card';


const IconBooleanProps = (baseTitle, value) => {
    if (!value) return null;
    return value === 'Yes' ? {
        name: 'check',
        color: 'green',
        title: `${baseTitle}: Yes`
    } : {
        name: 'x',
        color: 'red',
        title: `${baseTitle}: No`
    }
}


const StudySummary = ({ data: study }) => {
    const [showFullDescription, setState] = useState(false);
    const { fetchedAt, ProtocolSection } = study;
    const { NCTId, OrgStudyIdInfo: orgIdInfo = {}, Organization: org = {}, } =
        ProtocolSection.IdentificationModule;
    const versionDate = ProtocolSection.StatusModule.LastUpdateSubmitDate;
    const descriptionsObj = ProtocolSection.DescriptionModule;

    let detailedDisp = null;

    if (descriptionsObj.DetailedDescription) {
        detailedDisp = showFullDescription ? <>
            <Header as="h4"> Full Description</Header>
            <p>{descriptionsObj.DetailedDescription}</p>
            <Button onClick={() => setState(!showFullDescription)} animated='fade'>
                <Button.Content hidden>Hide</Button.Content>
                <Button.Content visible>
                    <Icon name='arrow circle up' />
                </Button.Content>
            </Button>
        </> : <Button onClick={() => setState(!showFullDescription)} animated='fade'>
            <Button.Content hidden>More</Button.Content>
            <Button.Content visible>
                <Icon name='eye' />
            </Button.Content>
        </Button>;
    }
    const { OversightModule: oversightInfo = {}, IPDSharingStatementModule = {} } = ProtocolSection;


    const ipdText = IPDSharingStatementModule.IPDSharing || 'Unknown';
    const { OrgStudyId: otherOrgId = '' } = orgIdInfo
    const { OrgFullName: orgName = '', OrgClass: orgClass = '' } = org;
    const hasDocumnets = study.DocumentSection ? study.DocumentSection.LargeDocumentModule.LargeDocList.LargeDoc.length : 0;



    const { IsFDARegulatedDevice, IsFDARegulatedDrug, OversightHasDMC } = oversightInfo;

    const fdaDrugProps = IconBooleanProps('Has a FDA Regulated Drug', IsFDARegulatedDrug);
    const fdaDeviceProps = IconBooleanProps('Has a FDA Regulated Device', IsFDARegulatedDevice);
    const fdaDMCProps = IconBooleanProps('Has Data Monitoring Comittee ', OversightHasDMC);

    const showGroup = !!fdaDrugProps || !!fdaDrugProps || !!fdaDMCProps;

    return <Segment textAlign="center">
        <Header as="h3">Summary</Header>
        <Header as="h4" size="small">{NCTId}
            {hasDocumnets > 0 ? <Label color="blue" content={hasDocumnets} icon="file text" size="medium" title="Check this study on clinicaltrials.gov for supporting documents" /> : null}
        </Header>
        <Container fluid>
            <Label title="Study Organisation">
                <Icon name="building outline" />
                <Label.Detail title="Organisation Study Identifier">{otherOrgId}</Label.Detail>
                <span> {orgName}</span>
                <Label.Detail title="Organisation type">{capitalize(orgClass)}</Label.Detail>
            </Label>
            <Label title="IPD sharing">
                {ipdText}
                <Label.Detail>
                    <Icon name="external share" />
                    <Icon name="users" />
                </Label.Detail>
            </Label>
            <Label color="green" title="Date of last study publication on ct.gov"> {versionDate}
                <Label.Detail>
                    <Icon name="database" />
                </Label.Detail>
            </Label>
            <Label title="Fetched At">
                {new Date(fetchedAt).toLocaleString()}
                <Label.Detail>
                    <Icon name="search" />
                    <Icon name="sync alternate" />
                </Label.Detail>
            </Label>
        </Container>
        {showGroup ?
            <Label.Group>
                <Header as="h4"> Oversight Information </Header>
                {!!fdaDrugProps ? <Label title={fdaDrugProps.title}>
                    <Icon size="big" name="pills" />
                    <Icon size="big" {...fdaDrugProps} />
                </Label> : null}
                {!!fdaDeviceProps ? <Label title={fdaDeviceProps.title}>
                    <Icon size="big" name="fax" />
                    <Icon size="big" {...fdaDeviceProps} />
                </Label> : null}
                {!!fdaDMCProps ? <Label title={fdaDMCProps.title}>
                    <Icon size="big" name="hdd" />
                    <Icon size="big" {...fdaDMCProps} />
                </Label> : null}
            </Label.Group> : 
            null}


        <Container fluid text textAlign="left">
            <Header as="h4">Description</Header>
            <p>{descriptionsObj.BriefSummary}</p>
            {detailedDisp}
        </Container>

        <a title="View on clinicaltrials.gov" rel="noreferrer" target="_blank" href={getExternalLink(NCTId)}>
            <Button size="medium" inverted color="blue" icon="external" />
        </a>
    </Segment>
}

export default StudySummary;
