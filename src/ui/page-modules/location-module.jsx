import _, { toLower } from 'lodash';
import React, { useState } from 'react';
import { Button, Card, Flag, Grid, Header, Icon, Label, List, Modal, Segment, Table } from 'semantic-ui-react';

const flatten = (data) => {
    const { OverallOfficialList: officialObj = {}, CentralContactList: cntObj = {}, LocationList: locObj = {} } = data;
    const { CentralContact: centralContacts = [] } = cntObj;
    const { Location: locations = [] } = locObj;
    const { OverallOfficial: officials = [] } = officialObj;
    return { locations, centralContacts, officials };
}

const iconSize = 'large';


class ContactModal extends React.Component {
    state = {
        contactMap: {}//determines what has been revealed
    }

    onToggleContactDetails = (idx, property) => {
        const contactMap = { ...this.state.contactMap };
        const contactRef = contactMap[idx] || {};
        const value = contactRef[property] || false;
        contactRef[property] = !value;
        contactMap[idx] = contactRef;
        this.setState({ contactMap });
    }

    arrayIsEqual = (x, y) => {
        return _(x).differenceWith(y, _.isEqual).isEmpty();
    }

    componentDidUpdate = (prevProps) => {
        const { contacts: prevContacts = [] } = prevProps
        const { contacts = [] } = this.props;
        if (prevContacts.length === 0 || contacts.length === 0) return;
        if (!this.arrayIsEqual(contacts, prevContacts)) {
            this.setState({ contactMap: {} });
        }
    }

    render() {
        const { context = 'CentralContact', contacts, title = 'Contacts', open, onDismiss } = this.props;
        const { contactMap } = this.state;

        return (<Modal basic dimmer="blurring" onClose={() => onDismiss()} open={open} size='small' >
            <Header textAlign="center" as="h1"> {title}</Header>
            <Segment inverted vertical textAlign="center">
                <Icon size="huge" color="yellow" name="warning sign" />
                <Header as="span" color="yellow"> You may only make contact for study-related queries (No Solicitation)</Header>
            </Segment>
            <Card.Group>
                {contacts.map((c, idx) => {
                    const name = c[`${context}Name`];
                    const displayState = contactMap[idx] || {};
                    let roleDisplay = null, emailDisplay = null, phoneDisplay = null;

                    const role = c[`${context}Role`];
                    const email = c[`${context}EMail`];
                    const phone = c[`${context}Phone`];
                    if (role) {
                        roleDisplay = <div style={{ marginTop: 6 }}>
                            <Icon color="blue" size="huge" name="user" />
                            <Label color="grey" title="Role">{role}</Label>
                        </div>;
                    }
                    else {
                        roleDisplay = <Icon className="fitted" color="blue" size="huge" name="user" />
                    }

                    if (email) {
                        emailDisplay = !!displayState.email ? <Label color="blue" title="Email" onClick={() => this.onToggleContactDetails(idx, 'email')} >
                            {/* <Icon color="blue" name="mail" /> */}
                            {email}
                        </Label> :
                            <Label className="pointer" title="View Email" onClick={() => this.onToggleContactDetails(idx, 'email')}>
                                <Icon size={iconSize} name="mail" />
                                <Icon size={iconSize} name="eye" />
                            </Label>;
                    }
                    if (phone) {
                        phoneDisplay = !!displayState.phone ? <Label color="blue" title="Phone" onClick={() => this.onToggleContactDetails(idx, 'phone')} >
                            {phone}
                        </Label> :
                            <Label className="pointer" title="View Phone" onClick={() => this.onToggleContactDetails(idx, 'phone')} >
                                <Icon size={iconSize} name="phone" />
                                <Icon size={iconSize} name="eye" />
                            </Label>;
                    }

                    return <Card key={`contact-modal-contact-${idx}`}>
                        <Grid stackable relaxed columns={16}>
                            <Grid.Row verticalAlign="bottom">
                                <Grid.Column width={1} />
                                <Grid.Column width={3}>
                                    {roleDisplay}
                                </Grid.Column>
                                <Grid.Column textAlign="center" verticalAlign="middle" width={10}>
                                    <Header as="h2" color="blue">  {name}</Header>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row centered verticalAlign="top">
                                <Grid.Column width={7}> {emailDisplay}</Grid.Column>
                                <Grid.Column width={7}>{phoneDisplay} </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Card>
                })}
            </Card.Group>
        </Modal>);
    }
}



const LocationModule = ({ data }) => {
    const { locations, centralContacts, officials } = flatten(data);

    const [{ contactType, locationIdx }, setState] = useState({ contactType: 'none', locationIdx: -1 });
    let contactsTitle = 'Contacts';
    let currentContacts = [];


    if (contactType === 'CentralContact') {
        currentContacts = centralContacts;

    }
    else if (contactType === 'LocationContact' && (locationIdx > -1 && locationIdx < locations.length)) {
        const currentLocation = locations[locationIdx]
        const facility = currentLocation.LocationFacility;
        contactsTitle = `Contacts in : ${facility.length > 80 ? facility.substr(0, 40) + '...' : facility}`;
        const { LocationContactList: contactListObj = {} } = currentLocation;
        ({ LocationContact: currentContacts = [] } = contactListObj);
    }
    
    return <Card fluid>
        <Card.Header textAlign="center" as="h3">Location and Contact Information</Card.Header>
        {centralContacts.length > 0 ?
            <Button primary onClick={() => setState({ contactType: 'CentralContact', locationIdx })} size="huge" animated>
                <Button.Content visible>
                    <Icon name='users' />
                </Button.Content>
                <Button.Content hidden>View General Study Contacts</Button.Content>
            </Button> : null}
        {locations.length > 0 ? <Table>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell textAlign="center">Country</Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">City</Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">Facility Name</Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">Zip code</Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">Recruiting Status</Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">Contacts</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {locations.map((location, idx) => {
                    const { LocationCity: city, LocationCountry: country, LocationContactList: conWrapObj = {},
                        LocationFacility: facility, LocationStatus: status, LocationZip: zip } = location;
                    const { LocationContact: contacts = [] } = conWrapObj;

                    const flag = country ? <Flag fluid title={`In ${country}`} name={toLower(country)} /> : null;

                    let contactIcon = null;

                    if (contacts.length === 1) {
                        contactIcon = <Button inverted onClick={(e) => setState({ contactType: 'LocationContact', locationIdx: idx })} title="Click to view center contact" icon="user" color="blue" size="big" />
                    }
                    else if (contacts.length > 1) {
                        contactIcon = <Button inverted onClick={(e) => setState({ contactType: 'LocationContact', locationIdx: idx })}
                            title="Click to view center contacts" icon="address book" color="blue" size="big" />
                    }
                    else {
                        contactIcon = <Icon title="No locational contacts available" name="user outline" color="grey" size="big" />
                    }

                    return <Table.Row key={`location-row-${idx}`}>
                        <Table.Cell textAlign="center">{flag}{country}</Table.Cell>
                        <Table.Cell textAlign="center">{city}</Table.Cell>
                        <Table.Cell textAlign="center">{facility}</Table.Cell>
                        <Table.Cell textAlign="center">{zip}</Table.Cell>
                        <Table.Cell textAlign="center">{status}</Table.Cell>
                        <Table.Cell textAlign="center">{contactIcon} </Table.Cell>
                    </Table.Row>
                })}
            </Table.Body>
        </Table> : null}
        {officials.length > 0 ?
        <Card.Content>
            <List celled>
                <List.Header as="h4"> Officials</List.Header>
                {officials.map((official, idx) => {
                    const { OverallOfficialName: name = '', OverallOfficialRole: role = '',
                        OverallOfficialAffiliation: affil = '' } = official;

                    return <List.Item key={`off-lst-itm-${idx}`}>
                        <span>{name}</span>
                        <br />
                        {role.length > 0 ? <Label>
                            <span>{role}</span>
                            <Label.Detail>Role</Label.Detail>
                        </Label> :
                            null}
                        {affil.length > 0 ? <Label>
                            <span>{affil}</span>
                            <Label.Detail>Affiliation</Label.Detail>
                        </Label> :
                            null}

                    </List.Item>

                })}
            </List>
            </Card.Content>
            : null}
        <ContactModal title={contactsTitle} onDismiss={() => setState({ locationIdx, contactType: 'none' })} context={contactType} open={contactType !== 'none'} contacts={currentContacts} />
    </Card>
}

export default LocationModule;