import { toLower } from 'lodash';
import React, { useState } from 'react';
import { Card,  Header, Icon, Input, Label } from 'semantic-ui-react';

const ConditionList = ({ data }) => {
    const { ConditionList: conditionObj = {}, KeywordList = {} } = data;
    const { Condition: conditions = [] } = conditionObj;
    const { Keyword: keywords = [] } = KeywordList;

    const [keyWordSearch = '', setState] = useState('');


    const normalisedSearch = toLower(keyWordSearch);
    const filteredWords = [];
    for (const row of keywords) {
        const phrases = row.split(',');
        if (keyWordSearch.length < 1) {
            filteredWords.push(...phrases);
            continue;
        }
        for (const phrase of phrases) {
            const normalisedPhrase = toLower(phrase);
            if (normalisedPhrase.includes(normalisedSearch) || normalisedSearch.includes(normalisedPhrase)) {
                filteredWords.push(phrase);
            }
        }
    }
    filteredWords.sort();

    return (<Card fluid textAlign="center">
        <Card.Header as="h2">Condition Summary</Card.Header>
        <Card.Content textAlign="center">
            {conditions.length > 0 ? conditions.map((c, idx) => <Label icon="medkit" key={`summary-lbl-${idx}`} size="huge" color="blue" > {c} </Label>) : <>
                <Icon color="red" name="ban" size="massive" />
                <Header textAlign="center"> No Conditions Found</Header>
            </>}
        </Card.Content>
        {keywords.length > 0 ?
            <Card.Content>
                <Card.Header as="h4">Keywords</Card.Header>
                {keywords.length>12? <Input icon="filter" value={keyWordSearch} onChange={(e,data) => setState(e.target.value)} />:null}
                <Card.Description>
                    {filteredWords.length > 0 ? filteredWords.map((phrase, idx) => <Label key={`phrase-${idx}`}>{phrase}</Label>) :
                        <>
                        <Icon name="ban" size="huge" color="red"/>
                        <Header as="h4">No Keywords Found</Header>
                        </>
                    }
                </Card.Description>
            </Card.Content>
            : null}
    </Card>)
}

export default ConditionList;
