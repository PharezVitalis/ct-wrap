import React from 'react';
import { Card,  Header,  Label, List } from 'semantic-ui-react';

const flatten = (data) => {
    const { ArmGroupList: armgGroupListObj = {}, InterventionList: interventionListObj = {} } = data;
    const { ArmGroup: armGroups = [] } = armgGroupListObj;
    const { Intervention: interventions = [] } = interventionListObj;
    return { armGroups, interventions };
}

const ArmInterventions = ({ data }) => {
    const { armGroups, interventions } = flatten(data);
    return <Card  className="mxa" fluid>
        <Card.Header textAlign="center" as="h2"> Intervention Details</Card.Header>
        {armGroups.length > 0 ?
            <Card.Content>
                <List verticalAlign="middle">
                    <List.Header textAlign="center" as="h3"> Arm Groups</List.Header>
                    {armGroups.map((aGroup, idx) => {
                        const { ArmGroupDescription: description, ArmGroupLabel: label } = aGroup;
                        return (<List.Item as="div" key={`arm-groups-${idx}`}>
                            <List.Icon name="eye"/>
                            <strong>{label}</strong>
                            <List.Item className="target" textAlign="center" padded="very" as="div"><em>{description}</em></List.Item>
                        </List.Item>);
                    })}
                </List>
            </Card.Content> : null
        }
        <List celled>
            <List.Header  textAlign="center" as="h3"> Interventions</List.Header>
            {interventions.map((interv, idx) => {
                const { InterventionArmGroupLabelList: labelsObj = {}, InterventionDescription: desc, InterventionType: type } = interv;
                const { InterventionArmGroupLabel: groupLabels = [] } = labelsObj;
                
                return <List.Item key={`interv-group-${idx}`}>
                    <Header as="h5">{desc}</Header>
                        {groupLabels.map((gr, lIdx) => <Label title="Arm Group For Intervention" color="green" key={`group-${idx}-lbl-${lIdx}`}>{gr}</Label>)}
                    
                    <Label title="Intervention Type" tag>{type}</Label>
                </List.Item>
            })}
        </List>
    </Card>
}

export default ArmInterventions;