import React from 'react';
import { Card, Icon, Label } from 'semantic-ui-react';
import { getFieldText } from '../../App';
import { TypeMap } from '../field-mappers';


const DesignModule = ({ data }) => {
    const { DesignInfo: summInfo } = data;
    const summaryLabels = [];

    if (summInfo.DesignMaskingInfo) {
        summaryLabels.push(<Label color="blue" size='big' key={`design-lbls-${summaryLabels.length}`} tag >
            {summInfo.DesignMaskingInfo.DesignMasking}
            <Label.Detail >
                Masking
            </Label.Detail>
        </Label>);
        const { DesignWhoMaskedList: dwmList = {} } = summInfo.DesignMaskingInfo;
        const { DesignWhoMasked: maskedRoles = [] } = dwmList;
        if (maskedRoles.length > 0) {

            summaryLabels.push(<Label key={`design-lbls-${summaryLabels.length}`} size="big" title="Roles that are masked" pointing="left">
                <Icon name="eye slash outline" />
                {maskedRoles.map((r,idx)=><span className="mx1" key={`masked-role-${idx}`} >{r}</span>)}
            </Label>)
        }
    }

    for (const key in summInfo) {
        if(key === 'DesignMaskingInfo')continue;
        const value = summInfo[key];
        const type = typeof value;
        const textValue = TypeMap[type](value);
       
        summaryLabels.push(<Label color="blue" size='big' key={`design-lbls-${summaryLabels.length}`} tag >
            {textValue}
            <Label.Detail >
                {getFieldText(key, true)}
            </Label.Detail>
        </Label>)
    }

    let phaseTxt = "Unknown";
    const { PhaseList = {} } = data;
    const { Phase = [] } = PhaseList;

    if (Phase.length > 0) {
        phaseTxt = Phase.join(', ')
    }

    const { EnrollmentInfo = {} } = data;
    const { EnrollmentCount = '(Unknown)', EnrollmentType = 'Unknown' } = EnrollmentInfo;

    return (<Card fluid>
        <Card.Content>
            <Card.Header textAlign="center" as="h3">
                Study Design Information
            </Card.Header>
            <Label title="Type of Study" size="large">
                {data.StudyType}
            </Label>
            <Label title="Trial Phases" size="large">
                {phaseTxt}
            </Label>
            <Label title="Participant Enrollment" size="large">
                <Icon name="users" />
                {EnrollmentCount}
                <Label.Detail title="Enrollment Type">
                    {`${EnrollmentType}`}
                </Label.Detail>
            </Label>
        </Card.Content>
        {/* <Card.Content></Card.Content> */}
        {summaryLabels.length > 0 ?
            <Card.Content>
                <Card.Header textAlign="center" as="h4"> Design Summary</Card.Header>
                {summaryLabels}
            </Card.Content>
            : null}
    </Card>)
}

export default DesignModule;