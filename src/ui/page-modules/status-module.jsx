import React from 'react';
import { Card, Icon, Label } from 'semantic-ui-react';
import { getFieldText } from '../../App';

const statusIconMap = {
    'Not yet recruiting': {
        name: "hourglass start",
        color: 'yellow'
    },
    'Recruiting': {
        name: "users",
        color: 'green'
    },
    'Enrolling by invitation': {
        name: 'mail outline',
        color: 'blue'
    },
    'Active, not recruting': {
        name: 'stopwatch',
        color: 'yellow'
    },
    'Suspended': {
        name: 'exclamation circle',
        color: 'yellow'
    },
    'Terminated': {
        name: "x",
        color: 'red'
    },
    'Completed': {
        name: "flag checkered",
        color: 'green'
    },
    'Withdrawn': {
        name: "ban",
        color: 'red'
    },
    'Unknown status': {
        name: 'question',
        color: 'yellow'
    }
}

const DateLabel = ({ value, parentKey = '' }) => {
    const isString = !parentKey.includes('Struct');
    let displayText = '', detail = '';

    if (isString) {
         detail = getFieldText(parentKey).replace(' Date', '');
         displayText = value;
    } 
    else {
        const baseKey = parentKey.replace('Struct', '');
        const dateValue = value[baseKey]
        const dateType = value[`${baseKey}Type`]
        displayText =dateType? `${dateValue} (${dateType})`:dateValue;
        detail =getFieldText(baseKey).replace(" Date",'')
    }

    return (<Label>
        <Icon name="calendar outline" />
        {displayText}
        <Label.Detail>{detail}</Label.Detail>
    </Label>)

}

const StatusModule = ({ data }) => {
    const { OverallStatus} = data;

    const dateLabels = [];

    for( const key in data){
        if (!key.includes('Date')) continue;
        dateLabels.push(<DateLabel parentKey={key}  value={data[key]} key={`date-labels-${dateLabels.length}`}/>)
    }

    return (<Card fluid>
        <Card.Content>
            <Card.Header textAlign="center" as="h3">
                Study Status
            </Card.Header>
            <Label size="huge">
            <Icon className="hover-view-parent" size="large" {...statusIconMap[OverallStatus]} />
                <span className="hover-target"> {OverallStatus} </span>
            </Label>
        </Card.Content>
        <Card.Content>
            <Label.Group color="blue" size="big">
                {dateLabels}
            </Label.Group>
        </Card.Content>
    </Card>)
}

export default StatusModule;