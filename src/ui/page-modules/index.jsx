import ConditionModule from './condition-module';
import BasicConditionModule from './basic-condition-module';
import ArmInterventions from './arm-interventions-module';
import LocationModule from './location-module';
import DesignModule  from './design-module';
import EligibilityModule from './eligibility-module';
import StatusModule from './status-module';
import OutcomesModule from './outcomes-module';
import StudySummary from './summary';


const moduleCompMap = {
    'DerivedSection.ConditionBrowseModule':ConditionModule,
    'ProtocolSection.ConditionsModule':BasicConditionModule,
    'ProtocolSection.ArmsInterventionsModule':ArmInterventions,
    'ProtocolSection.ContactsLocationsModule':LocationModule,
    'ProtocolSection.DesignModule':DesignModule,
    'ProtocolSection.EligibilityModule':EligibilityModule,
    'ProtocolSection.StatusModule':StatusModule,
    'ProtocolSection.OutcomesModule':OutcomesModule,
    '!Summary':StudySummary // ! - denotes that the whole study should be passed as a prop for that module
}

export const moduleDropdownMap = [
    { key:"conditions-simple", text:"Simple Conditions List" ,value:'ProtocolSection.ConditionsModule'},
    { key:"conditions", text:"Medical Condition References" ,value:'DerivedSection.ConditionBrowseModule'},
    { key:"arm-interventions", text:"Arm interventions" ,value:'ProtocolSection.ArmsInterventionsModule'},
    { key:"location", text:"Location and Contact" ,value:'ProtocolSection.ContactsLocationsModule'},
    {key:"study-design", text:"Study Design Info" ,value:'ProtocolSection.DesignModule'},
    {key:"eligibility", text:"Participant Eligibility" ,value:'ProtocolSection.EligibilityModule'},
    {key:"status", text:"Study Status" ,value:'ProtocolSection.StatusModule'},
    {key:"outcomes", text:"Study Outcomes" ,value:'ProtocolSection.OutcomesModule'},
    {key:"summary", text:"Summary" ,value:'!Summary'}
]

export default moduleCompMap;