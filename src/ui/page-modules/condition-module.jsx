import { capitalize } from 'lodash';
import React, { useState } from 'react';
import { Card, Dropdown, Label } from 'semantic-ui-react';

const flatten = (data) => {
    const { ConditionAncestorList = {}, ConditionBrowseBranchList = {},
        ConditionBrowseLeafList = {}, ConditionMeshList = {} } = data;

    const ancestorList = ConditionAncestorList.ConditionAncestor || [];
    const branchList = ConditionBrowseBranchList.ConditionBrowseBranch || [];
    const leafList = ConditionBrowseLeafList.ConditionBrowseLeaf || [];
    const meshList = ConditionMeshList.ConditionMesh || [];

    return { ancestorList, branchList, leafList, meshList };
}

const listViewTypes = [{ key: 'ancestor', value: 'ancestor', text: "Ancestor List" }, { key: 'mesh', value: 'mesh', text: "MeSh List" }, { key: 'branch', value: 'branch', text: "Branch List" },
{ key: 'leaf', value: 'leaf', text: "Leaf List" }, { key: "all", value: 'all', text: "All" }]

const ConditionModule = ({ data }) => {
    const { ancestorList, branchList, leafList, meshList } = flatten(data);
    const [listType, setListType] = useState(listViewTypes[0].value);



    return <Card fluid raised centered >
        <Card.Content textAlign="center">
            <Card.Header textAlign="center" as="h3"> Medical Condition References</Card.Header>
            <Dropdown className="icon" floating buticon="eye" options={listViewTypes} value={listType} onChange={(e, data) => setListType(data.value)} />
        </Card.Content>
        {listType === 'ancestor' || listType === "all" ? <Card.Content textAlign="center">
            <Card.Header textAlign="center" as="h5"> Ancestor List</Card.Header>
            {ancestorList.map((item, idx) => {
                const { ConditionAncestorId: id, ConditionAncestorTerm: term } = item;

                return <Label size="big" key={`an-list-lbl-${idx}`}>
                    {term}
                    <Label.Detail as="h5"><em> Id: {id}</em> </Label.Detail>
                </Label>
            })}
        </Card.Content>
            : null}

        {listType === 'branch' || listType === "all" ?
            <Card.Content textAlign="center">
                <Card.Header textAlign="center" as="h4"> Branch List</Card.Header>
                {branchList.map((item, idx) => {
                    const { ConditionBrowseBranchAbbrev: abbrev, ConditionBrowseBranchName: name } = item;
                    return <Label size="big" key={`br-list-lbl-${idx}`}>
                        {name}
                        <Label.Detail as="h5"> <em>abbreviation: {abbrev}</em> </Label.Detail>
                    </Label>
                })}
            </Card.Content>
            : null}
       {listType === 'leaf' || listType === "all" ?     
        <Card.Content textAlign="center">
            <Card.Header textAlign="center" as="h4"> Leaf List</Card.Header>
            {leafList.map((item, idx) => {
                //ConditionBrowseLeafAsFound?
                const { ConditionBrowseLeafId: id, ConditionBrowseLeafName: name, ConditionBrowseLeafRelevance: rel } = item;
                return <Label textAlign="center" size="big" key={`lf-list-lbl-${idx}`}>
                    {name}
                    <Label.Detail as="h5"> <em>id: {id}</em> </Label.Detail>
                    <Label.Detail> <em style={{fontSize:13}}>relavance: {capitalize(rel)}</em> </Label.Detail>
                </Label>
            })}
        </Card.Content>
         : null}

         {listType === 'mesh' || listType === "all" ?
        <Card.Content textAlign="center">
            <Card.Header textAlign="center" as="h4"> Mesh List</Card.Header>
            {meshList.map((item, idx) => {
                //ConditionBrowseLeafAsFound?
                const { ConditionMeshId: id, ConditionMeshTerm: term } = item;
                return <Label textAlign="center" size="big" key={`ms-list-lbl-${idx}`}>
                    {term}
                    <Label.Detail as="h5"> <em>id: {id}</em> </Label.Detail>
                </Label>
            })}
        </Card.Content>
        : null}

    </Card>

}

export default ConditionModule;
