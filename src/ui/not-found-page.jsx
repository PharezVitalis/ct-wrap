import React from 'react';
import { Button,  Header, Icon, Segment } from 'semantic-ui-react';
import { A } from 'hookrouter';
import PageHeader from './page-header';

const NotFoundPage = () => {

    return <>
     <PageHeader />
     <Segment raised textAlign="center">
        <Icon color="red" size="massive" name="ban" />
        <Header>404 - Page Not Found</Header>
        <A href="/"><Button compact icon="home" size="huge" inverted color="blue" /></A>
    </Segment>
    </>
}

export default NotFoundPage;