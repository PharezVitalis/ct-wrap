import { Header, Icon, Segment } from "semantic-ui-react";
import { A } from 'hookrouter';

const PageHeader = () => {
    const {origin, href} = window.location;
    const showLink = href.length - origin.length > 1;
  
    const homeSection = showLink? <A  href='/'><Icon fitted={false} size="big" name="home" color="blue" /></A> :
    <span><Icon fitted={false} size="big" name="home" color="blue" /></span> ;

    return (<Segment textAlign="center" basic inverted>
        <Segment basic raised compact floated="left" >{homeSection} </Segment>        
        <Segment  basic textAlign="center" inverted vertical compact clearing> 
            <Header  as="h1">A  Wrapper for <a title="Go to ct.gov" rel="noreferrer" target="_blank" href="https://clinicaltrials.gov">Clinical Trials. gov <Icon name="external" /></a> </Header>
            <Header as="h5">Note this communicates directly with clinicaltrials.gov via live published information, this site is not affiliated with clinicaltrials.gov in any way. This site does not collect any personal data, it is all stored locally </Header>
        </Segment>
    </Segment>);
}
export default PageHeader