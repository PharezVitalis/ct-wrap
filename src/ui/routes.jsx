import HomePage from './home-page';
import React from 'react';
import SearchResultsPage from './search-results-page';
import StudyPage from './study-page';
import { useRoutes} from 'hookrouter';
import NotFoundPage from './not-found-page';

const routes = {
    "/":()=><HomePage />,
    '/search':()=><SearchResultsPage />,
    '/study/:NCTId':({NCTId})=><StudyPage NCTId={NCTId} />
}

const Router = ()=>{
    const match = useRoutes(routes);
    return match || <NotFoundPage />
}

export default Router;
