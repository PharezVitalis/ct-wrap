import StreamSaver from 'streamsaver';
import { ReadableStream } from 'web-streams-polyfill/ponyfill';
const fs = require('fs');
const jsonexport = require("jsonexport/dist")
const fetch = require('node-fetch');

export default class FileDownloader{
    constructor(url, count,path=null){
        this.url = url;
        this.count = count? count:0;
        this.path = path? path: 'ct-export';
    }

}