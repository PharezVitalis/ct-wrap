import Joi from "joi"
import Qs from 'query-string';
import { AllFieldValues, getFieldText } from "../App";
import { WritableStream } from "web-streams-polyfill/ponyfill/es2018";
import StreamSaver from 'streamsaver';
import { ceil, isArray,  round, toLower } from "lodash";
import * as CQ from 'country-codes-list';

const MIN_REQ_DELAY = 250;//how many ms min between 2 requests;
let lastRequestTime = Date.now();//done because once page is loaded, a request is fired immediately
const EXCEL_MAX_ROWS = 1048575;


//const fs = require('fs');
const Xlsx = require('xlsx');
const XlsxUtilities = Xlsx.utils;
const jsonexport = require("jsonexport/dist")
const https = require('https');
const fetch = require('node-fetch');
//should change the type 
StreamSaver.WritableStream = WritableStream;
let activeStreams = [];
const countryMap = CQ.customList('countryNameEn', '{officialLanguageNameEn}');

countryMap['Russian Federation'] = countryMap['Russia'];
countryMap['United States'] = countryMap['United State of America'];
countryMap['Korea, Republic of'] = countryMap['South Korea'];
countryMap['Taiwan'] = "Mandarin";
countryMap['Iran, Islamic Republic of'] = countryMap['Iran'];
countryMap['Hong Kong'] = "Mandarin";
countryMap['China'] = "Mandarin";
countryMap['Singapore'] = "Malay";
countryMap['Spain'] = "Spanish, Castilian"
countryMap['Czech Republic'] = countryMap['Czech'];
countryMap['Tanzania'] = countryMap['United Republic of Tanzani'];

export const connectionStatuses = ['initialised', 'retrieving']

export const searchSchema = Joi.object().keys({
    searchExpression: Joi.string().min(0).allow(''),
    fields: Joi.array().items(Joi.string().min(1).required()),
    skip: Joi.number().integer().min(0),
    perPage: Joi.number().integer().min(0).allow(null).max(100),
    format: Joi.string().allow("json", "xml", "csv")
})

export const performRequest = async (reqQuery, callback) => {
    if (lastRequestTime !== null) {
        const diffMs = Date.now() - lastRequestTime;
        if (diffMs < MIN_REQ_DELAY) {
            await sleep(MIN_REQ_DELAY - diffMs);
        }
    }
    lastRequestTime = Date.now();
    const request = https.get(reqQuery, (response) => {
        let buffer = '';
        response.on('data', chunk => buffer += chunk);
        response.on('end', (err) => {
            callback(err, buffer);
        })
    });
    return request;
}

export const searchToQueryString = (searchState, currentPage = 1, requestType = "study_fields") => {
    const { searchField = {}, searchExpression = '', fields, skip, perPage = 100, format: fmt = "json" } = searchState;
    let expr = searchExpression.trim().replace(' ', '+') || '';
    if (expr.length < 1) {
        expr = "ALL"
    }
    const searchFieldKeys = Object.keys(searchField);
    for (const key of searchFieldKeys) {
        const fieldRefObj = AllFieldValues.find(v => v.value === key);

        const { area } = fieldRefObj;
        if (!area) {
            const addCoverage = toLower(key).includes('status');//this is done so that "Recruiting" and "Not yet recruting" aren't found together. But a condition to do with the "heart" should not be an exact match;
            expr += ` AND (AREA[${key}]${addCoverage?'COVERAGE[FullMatch]':''}${searchField[key]})`;
            continue;
        }
        const areaIdx = expr.indexOf(area);
        if (areaIdx < 0) {
            expr += ` AND SEARCH[${area}](AREA[${key}]${searchField[key]})`;
        }
        else {
            const startIdx = areaIdx + area.length + 2; //Some Search AND SEARCH[Location](Area[Property]Value and Area[OtherRelevantProperty]Value)
            const nextExpr = expr.substr(0, startIdx) + `AREA[${key}]${searchField[key]} AND ` + expr.substr(startIdx);
            expr = nextExpr;
        }
    }

    let min_rnk = skip > 0 ? skip + 1 : 1;
    if (currentPage > 1) {
        min_rnk += (currentPage - 1) * perPage;
    }
    const max_rnk = min_rnk - 1 + perPage;
    const baseUrl = `https://clinicaltrials.gov/api/query/${requestType}?`;

    const queryString = Qs.stringify({ expr, min_rnk, max_rnk, fields, fmt }, {
        arrayFormat: 'comma',
        skipEmptyString: true,
        encode: false
    });

    return baseUrl + queryString;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export const fileFormats = ['csv', 'xlsx'];

export const defaultQueryOptions = {
    formatOptions: {},
    requestType: "study_fields", fileFormat: 'csv', methods: {}
}

const defaultFileFormatOptions = {
    'csv': ({ headersText }) => {
        return {
            rename: headersText,
            typeHandlers: {
                String: (value) => value.trim(),
                Boolean: value => value ? 'Yes' : 'No',
                Array: value => {
                    if (value.length === 1) return value[0]///recusive for strings and booleans withing arrays?
                    const firstStr = (value[0] || '').toString().trim();
                    const valueMap = { [firstStr]: true };;
                    let outStr = firstStr;

                    for (let i = 1; i < value.length; i++) {
                        const nextStr = value[i].toString().trim();
                        if (valueMap[nextStr]) {
                            continue;
                        }
                        valueMap[nextStr] = true;
                        outStr += (outStr.length > 0 ? ' || ' : '') + nextStr.trim();
                    }
                    return outStr;
                }
            }
        };
    },
    'xlsx': ({ headers, headersText }) => {
        const firstRow = {};

        for (let i = 0; i < headers.length; i++) {
            const header = headers[i];
            const headerText = headersText[i];
            firstRow[header] = headerText;
        }

        return {
            firstRow,
            bookType: 'xlsx',
            header: headers,
            skipHeader: true,
            addCountryData: false,
            addStudyLocData: false
        };
    }
}

const isSimpleFormat = (fmt) => fmt !== 'xlsx';

const excelJsonFormatter = (values) => {
    const outFormatted = {};
    for (const key in values) {
        const value = values[key];

        if (!isArray(value) || typeof value === 'string') {
            outFormatted[key] = value.toString().trim();
            continue;
        }

        if (value.length === 0) {
            outFormatted[key] = '';
        }
        else if (value.length === 1) {
            outFormatted[key] = value[0].toString().trim();
        }
        else {
            const occuredValues = {};
            let cellString = value[0].toString().trim();
            occuredValues[cellString] = true;
            for (let i = 1; i < value.length; i++) {
                const nextValue = ("" + value[i]).trim();
                if (occuredValues[nextValue]) {
                    continue;
                }
                occuredValues[nextValue] = true;
                cellString += ' || ' + value[i];
            }
            outFormatted[key] = cellString;
        }
    }
    return outFormatted;
}

const updateGeoOccurances = (studies = [], countryOccurances = {}, languageOccurances = {}, addStudyLocData = false) => {

    for (const study of studies) {
        const { LocationCountry = [] } = study;
        let stCountryCount = 0;
        const studyLanguages = {};

        if (LocationCountry.length === 1) {
            stCountryCount = 1;
            
            const country = LocationCountry[0];
            let countryCount = countryOccurances[country] || 0;
            countryCount++;
            countryOccurances[country] = countryCount;
            const langKey = countryMap[country];
            if (!langKey) {
                continue;
            }
            let langCount = languageOccurances[langKey] || 0;
            langCount++;
            languageOccurances[langKey] = langCount
            studyLanguages[langKey] = true;
        }
        else if (LocationCountry.length > 1) {
            const countryTracker = {};
            for (const country of LocationCountry) {
                if (countryTracker[country]) continue;
                stCountryCount++;
                countryTracker[country] = true;
                let countryCount = countryOccurances[country] || 0;
                countryCount++;
                countryOccurances[country] = countryCount;
                const langKey = countryMap[country];
                if (!langKey) {
                    continue;
                }
                let langCount = languageOccurances[langKey] || 0;
                langCount++;
                languageOccurances[langKey] = langCount;
                studyLanguages[langKey] = true;
            }
        }
        
        const languageKeys = Object.keys(studyLanguages);
        let languageStr = ''

        if(languageKeys.length>0){
            languageStr = languageKeys[0];
            for(let i = 1; i<languageKeys.length; i++){
                languageStr += ` || ${languageKeys[i]}`;
            }
        }

        if (addStudyLocData) {
            study.countryCount = "" + stCountryCount;
            study.languages = languageStr;
            study.languageCount = "" + languageKeys.length;
        }
    }
   
    return { languageOccurances, countryOccurances }
}

const jsonToFile = {
    'csv': async (data, formatOptions = {}, writeStream, onProgress, query) => {
        const writer = writeStream.getWriter();
        const { StudyFields: studies = [], NStudiesFound: total, NStudiesReturned} = data.StudyFieldsResponse;;
        if (total > EXCEL_MAX_ROWS) {
            throw new Error(`Only up to ${EXCEL_MAX_ROWS} allowed in an excel spreadsheet`)
        }
        const csv = await jsonexport(studies, formatOptions);
        const pages = ceil((total - NStudiesReturned) / 1000);
        onProgress(round(1 / pages * 100));
        const encoder = new TextEncoder();
        const encoded = encoder.encode(csv);
        writer.write(encoded);
        formatOptions.includeHeaders = false;
        for (let i = 0; i < pages; i++) {
            const diffMs = Date.now() - lastRequestTime;
            if (diffMs < MIN_REQ_DELAY) {
                await sleep(MIN_REQ_DELAY - diffMs);
            }
            query.skip += 1000;
            const nextUrl = searchToQueryString(query);
            lastRequestTime = Date.now();
            const nextRequest = await fetch(nextUrl);
            const nextStudySetObj = await nextRequest.json();
            const { StudyFields: nextStudies = [] } = nextStudySetObj.StudyFieldsResponse;
            const nextSet = await jsonexport(nextStudies, formatOptions);
            onProgress(round((i + 2) / pages * 100));
            writer.write(encoder.encode('\n' + nextSet));
        }
        writer.close();
    },
    'xlsx': async (data, formatOptions = {}, writeStream, onProgress, query) => {
        const { StudyFields: studies = [], NStudiesFound: total, NStudiesReturned } = data.StudyFieldsResponse;;

        const pages = ceil((total - NStudiesReturned) / 1000);
        const { firstRow, addCountryData = false, addStudyLocData = false } = formatOptions;

        const countryOccurances = {};
        const languageOccurances = {};

        if (addCountryData) {
            if (addStudyLocData) {
                const { header } = formatOptions;
                header.push('languages','countryCount', 'languageCount');
                firstRow.languages = 'Languages'
                firstRow.countryCount = "Country Count";
                firstRow.languageCount = "Language Count";
            }
            updateGeoOccurances(studies, countryOccurances, languageOccurances, addStudyLocData);
        }
        const formattedStudies = studies.map(v => excelJsonFormatter(v));
        onProgress(round(1 / pages * 100));
        formattedStudies.unshift(firstRow);

        const sheet = XlsxUtilities.json_to_sheet(formattedStudies, formatOptions);
        formatOptions.origin = -1;

        for (let i = 0; i < pages; i++) {
            const diffMs = Date.now() - lastRequestTime;
            if (diffMs < MIN_REQ_DELAY) {
                await sleep(MIN_REQ_DELAY - diffMs);
            }
            query.skip += 1000;
            const nextUrl = searchToQueryString(query);
            lastRequestTime = Date.now();
            onProgress(round((i) / pages * 100));
            const nextRequest = await fetch(nextUrl);
            onProgress(round((i + 1) / pages * 100));
            const nextStudySetObj = await nextRequest.json();
            const { StudyFields: nextStudies = [] } = nextStudySetObj.StudyFieldsResponse;
            if (addCountryData) {
                updateGeoOccurances(nextStudies, countryOccurances, languageOccurances);
            }
            const nextStudiesFormatted = nextStudies.map(v => excelJsonFormatter(v));
            XlsxUtilities.sheet_add_json(sheet, nextStudiesFormatted, formatOptions);
        }
        const wb = Xlsx.utils.book_new();
        Xlsx.utils.book_append_sheet(wb, sheet, 'Studies');
        if (addCountryData) {
            const countryRows = [];
            for (const country in countryOccurances) {
                countryRows.push({
                    Country: country,
                    Count: countryOccurances[country]
                });
            }
            const langRows = [];
            for (const lang in languageOccurances) {
                langRows.push({ Language: lang, Count: languageOccurances[lang] })
            }
            const geoSheet = XlsxUtilities.json_to_sheet(countryRows, { header: ['Country', 'Count'] });
            XlsxUtilities.sheet_add_json(geoSheet, langRows, { origin: 'D1', header: ['Language', 'Count'] })
            Xlsx.utils.book_append_sheet(wb, geoSheet, 'Country Stats');
        }
        Xlsx.writeFile(wb, 'ct-wrap export.xlsx', { bookType: 'xlsx' });
    }
}

export const queryToFile = async (query, queryOpts = {}) => {
    let { formatOptions = {},  fileFormat = 'csv', methods = {} } = queryOpts; //requestType = "study_fields",

    query.perPage = 1000;
    const { onProgress, onComplete, onError } = methods;
    const url = searchToQueryString(query);

    const withWriter = isSimpleFormat(fileFormat);
    let writeStream = isSimpleFormat(fileFormat) ?
        StreamSaver.createWriteStream(`ct-wrap-export.${fileFormat}`, { writableStrategy: undefined, readableStrategy: undefined }) : null;

    let writeStreamIdx = activeStreams.length - 1;
    let err = null;
    try {
        const diffMs = Date.now() - lastRequestTime;
        if (diffMs < MIN_REQ_DELAY) {
            await sleep(MIN_REQ_DELAY - diffMs);
        }
        const firstRequest = await fetch(url);
        lastRequestTime = Date.now();
        const studyObj = await firstRequest.json();
        let headersText = ['Search Rank', 'NCT Id', 'Brief Title', 'Last Update Submit Date'];

        const { FieldList: fieldList } = studyObj.StudyFieldsResponse;

        const headers = ['Rank', ...fieldList];

        for (let i = 3; i < fieldList.length; i++) {
            const nextField = fieldList[i];
            headersText.push(getFieldText(nextField));
        }
        const mergedFormatOptions = { ...defaultFileFormatOptions[fileFormat]({ headersText, headers }), ...formatOptions };
        await jsonToFile[fileFormat](studyObj, mergedFormatOptions, writeStream, onProgress, query);
        if (withWriter) {
            activeStreams.splice(writeStreamIdx, 1)
        }
    }
    catch (e) {
        err = e;
        onError(e);
        if (writeStream) {
            writeStream.abort();
            if (writeStreamIdx > -1) {
                activeStreams.splice(writeStreamIdx, 1)
            }
        }
        return
    }
    finally {
        onComplete(err, !err);
    }
}


window.onbeforeunload = (e) => {
    if (activeStreams.length < 1) return;
    e.returnValue = 'Downloads have not yet finished, are you sure you want to leave?';
};

window.onunload = () => {
    if (activeStreams.length < 1) return;
    for (let i = 0; i < activeStreams.length; i++) {
        const str = activeStreams[i];
        if (!str) {
            activeStreams.splice(i, 1);
            continue;
        }
        else {
            str.close();
        }
    }
};