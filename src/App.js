import 'semantic-ui-css/semantic.min.css';
import './App.css';
import { Container, Loader, Segment } from 'semantic-ui-react';
import React from 'react';
import { FieldsContext } from './contexts';
import ErrorPage from './ui/error-page';
import fetch from 'node-fetch';
import Routes from './ui/routes';

const getUrlFromPub = (filename) => `${window.origin}/${filename}`;

//const fieldUrl = 'https://clinicaltrials.gov/api/info/study_fields_list';

const includedAreaStructs = ['Study', 'Location'];

const getHeaderData = async () => {
  const fieldListData = await (await fetch(getUrlFromPub('field-list.json'))).json();
  //  const studyStructureData = await(await fetch(getUrlFromPub('study-structure'))).json();
  const searchAreasData = await (await fetch(getUrlFromPub('search-areas.json'))).json();
  const { Docs = [] } = searchAreasData.SearchAreas;
  const searchAreas = [];

  for (const doc of Docs){
    if(!includedAreaStructs.includes(doc.StructName)) continue;
    const {Areas = [] } = doc;
    if(Areas.length<1) continue;
    searchAreas.push(...Areas);
  }

  const basicSearchIdx  = searchAreas.findIndex(a=>a.Name ==='BasicSearch');
  const basicSearchArea = searchAreas[basicSearchIdx];
  searchAreas.splice(basicSearchIdx, 1);
   //remove basic search as it uses a lot of the other areas;

  return { fieldList: fieldListData.StudyFields.Fields, searchAreas, basicSearchArea};
}


export let AllFieldValues = [];

export const getFieldText = (fieldKey, skipFirst = false) => {
  const words = fieldKey.match(/([A-Z]{1}[a-z]+)|[A-Z]+(?=[A-Z]{1}[a-z]+)/g);//EGForExample => [EG, For, Example];
  if (!skipFirst) return words.join(" ");
  if (words.length < 2) {
    return words.join(' ');
  }

  let str = words[1].trim();

  for (let i = 2; i < words.length; i++) {
    str += ` ${words[i].trim()}`;
  }
  return str;

}

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {

      failed: false,
      status: "Getting CT Header Data",
      apiVersion: 'Unknown'
    }

  }


  componentDidMount() {
    this.request = getHeaderData().then(fieldData => {
      this.setState({ status: 'Stitching Header Data' })
      const { fieldList, basicSearchArea,searchAreas } = fieldData;
      
      AllFieldValues = fieldList.map(value => {
        const text = getFieldText(value);

       
        
        let areaObj = searchAreas.find(({ Parts = [] }) => !!Parts.find(p => p.FieldName === value));
        
        let area = null;

        if(areaObj){
          const {Name:name} = areaObj;
          if(name.includes('Location')){
            area = "Location";
          }
          else{
            if(!basicSearchArea.Parts.find(p=>p.FieldName === value)){
              area = name;
            }
          }
        }
        
        return { text, area, value }
      })
      this.setState({ status: 'Loaded' })
    });
  }

  componentWillUnmount() {
    if (this.request) {
      this.request.destroy()
    }
  }

  render() {
    const { status } = this.state;
    if (status !== 'Loaded') {
      return (<ErrorPage>
        <Container fluid textAlign="center">
          <Segment >
            <Loader inline="centered" size="massive" active> {status} </Loader>
          </Segment>
        </Container>
      </ErrorPage>)
    }

    return <>
      <ErrorPage>
        <FieldsContext.Provider value={AllFieldValues}>
          <Routes />
        </FieldsContext.Provider>
      </ErrorPage>
    </>
  }
}




export default App;
